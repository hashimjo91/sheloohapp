package com.example.Config;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;

/**
 * Created by 2266 on 25/04/16.
 */
public class ResponceCoreFilter implements ContainerResponseFilter {
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        MultivaluedMap<String,Object> header=containerResponseContext.getHeaders();
        header.add("Access-Control-Allow-Origin","*");
        header.add("Access-Control-Allow-Method","GET, POST, DELETE, PUT");
        header.add("Access-Control-Allow-Headers","X-Requested-With, Content-Type, X-Codingpedia");
    }
}
