package com.example.Config;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.sse.SseFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Created by 2266 on 25/04/16.
 */
public class ServiceConfiguration extends ResourceConfig {
    public ServiceConfiguration() {
        super(
                GsonMessageBodyHandler.class,
                ResponceCoreFilter.class,
                SseFeature.class,
                MultiPartFeature.class);
    }
}
