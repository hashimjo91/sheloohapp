package com.example.Utilities;

/**
 * Created by Hashim Al-husban
 * on 26/04/16.
 */
public class ResponseStatus {
    public static final int SUCCESSFUL=1;
    public static final int FAILED = 5;
    public static final int WRONGUSER=2;
    public static final int WRONGPASS=3;
    public static final int INACTIVE=4;

}
