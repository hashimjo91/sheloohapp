package com.example.jersey;

import com.example.DataBase;
import com.example.Objects.User;
import com.example.Utilities.MD5Hash;
import com.example.Utilities.ResponseStatus;
import com.example.Utilities.Tags;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Hashim Al-husban
 * on 25/04/16.
 */
@Path("/Users")
public class Register {

    private final MD5Hash md5Hash = new MD5Hash();

    @POST
    @Path("/Register")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response RegUser(String user1) {
        User user = new User(user1);
        DataBase db = DataBase.getInstance();

        int i = db.InsertUser(user.getLanguage() == null ? Tags.USERINSERT : Tags.USERINSERTWITHLANG, user.getInsertMethod());
        return Response.status(ResponseStatus.SUCCESSFUL).entity(i == 0 ? "User Add failed" : "User Added Successfully").build();
    }

    @POST
    @Path("Login")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response Login(String Data) {
        JSONObject data = new JSONObject(Data);
        String Email = data.getString("Email");
        String pass = data.getString("Password");
        DataBase db = DataBase.getInstance();
        User u = db.GetUserInfo(Email);
        if (u == null) return Response.status(ResponseStatus.WRONGUSER).entity("Wrong Email or Password").build();
        if (u.getIsActive() == 1) {
            if (md5Hash.Hash(pass).equals(u.getPassword()))
                return Response.status(ResponseStatus.SUCCESSFUL).entity(u.toString() + "").build();
            else
                return Response.status(ResponseStatus.WRONGPASS).entity("Wrong Email or Password").build();
        }

        return Response.status(ResponseStatus.INACTIVE).entity("Account is not verified! please check your email").build();
    }


    @POST
    @Path("GetUserInfo")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response GetUserInfo(String Data) {
        JSONObject data = new JSONObject(Data);
        String Email = data.getString("Email");
        DataBase db = DataBase.getInstance();
        User u = db.GetUserInfo(Email);
        if (u == null) return Response.status(ResponseStatus.WRONGUSER).entity("Wrong Email or Password").build();
        if (u.getIsActive() == 1) {
            return Response.status(ResponseStatus.SUCCESSFUL).entity(u.toString() + "").build();
        }

        return Response.status(ResponseStatus.INACTIVE).entity("Account is not verified! please check your email").build();
    }


    @POST
    @Path("UpdateUserInfo")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response UpdateUserInfo(String Data) {
        JSONObject data = new JSONObject(Data);
        User i = new User();
        try {
            i = DataBase.getInstance().GetUserInfo(data.getString("Email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        i.ValidateData(Data);
        DataBase db = DataBase.getInstance();
        if (db.UpdateUserInfo(i) == 1) {
            return Response.status(ResponseStatus.SUCCESSFUL).entity(i.toString() + "").build();
        } else {
            return Response.status(ResponseStatus.FAILED).entity(i.toString() + "").build();

        }


    }

    @POST
    @Path("UpdateUser")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response UpdateUser(String Data) {
        DataBase db = DataBase.getInstance();
        JSONObject data = new JSONObject(Data);
        User i = db.GetUserInfo(data.getString("Email"));
        System.out.println(i.toString());
        i.setPassword(md5Hash.Hash(i.getPassword()));
        if (db.UpdateUser(i) == 1) {
            return Response.status(ResponseStatus.SUCCESSFUL).entity(i.toString() + "").build();
        } else {
            return Response.status(ResponseStatus.FAILED).entity(i.toString() + "").build();

        }


    }


}
