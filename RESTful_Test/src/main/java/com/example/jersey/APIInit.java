package com.example.jersey;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Hashim Al-husban
 * on 25/04/16.
 */
@Path("/init")
public class APIInit {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String initDB() {

        String tag = null;
        Connection c;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:d:\\test.db");

            Statement stm = c.createStatement();

            String sql = "CREATE TABLE IF NOT EXISTS Users\n" +
                    "(\n" +
                    "    ID INTEGER NOT NULL,\n" +
                    "    Email TEXT,\n" +
                    "    Password TEXT,\n" +
                    "    FirstName TEXT,\n" +
                    "    LastName TEXT,\n" +
                    "    Phone TEXT,\n" +
                    "    IsActive INTEGER,\n" +
                    "    Gender INTEGER,\n" +
                    "    Country INTEGER,\n" +
                    "    Age INTEGER,\n" +
                    "    FOREIGN KEY (Gender) REFERENCES Gender_Lookup (G_id) ON DELETE SET NULL ON UPDATE SET NULL DEFERRABLE INITIALLY DEFERRED,\n" +
                    "    FOREIGN KEY (Country) REFERENCES country (COUNTRY_NO) ON DELETE SET NULL ON UPDATE SET NULL DEFERRABLE INITIALLY DEFERRED\n" +
                    ");";

            stm.execute(sql);

            sql = "CREATE TABLE IF NOT EXISTS country\n" +
                    "(\n" +
                    "    COUNTRY_NO INT PRIMARY KEY NOT NULL,\n" +
                    "    COUNTRY_NAME TEXT NOT NULL,\n" +
                    "    COUNTRY_NAME_S TEXT NOT NULL,\n" +
                    "    NATIONALITY TEXT NOT NULL,\n" +
                    "    NATIONALITY_S TEXT NOT NULL\n" +
                    ");\n" +
                    "CREATE UNIQUE INDEX country_COUNTRY_NO_uindex ON country (COUNTRY_NO)";

            stm.execute(sql);

            sql = "CREATE TABLE IF NOT EXISTS Gender_Lookup\n" +
                    "(\n" +
                    "    G_id INTEGER PRIMARY KEY NOT NULL,\n" +
                    "    Gender_Name TEXT,\n" +
                    "    Gender_Name_S TEXT\n" +
                    ");\n" +
                    "CREATE UNIQUE INDEX Gender_Lookup_G_id_uindex ON Gender_Lookup (G_id);";

            stm.execute(sql);

            sql = "CREATE TABLE IF NOT EXISTS Permission_Lookup\n" +
                    "(\n" +
                    "    P_id INTEGER PRIMARY KEY NOT NULL,\n" +
                    "    P_Name TEXT NOT NULL,\n" +
                    "    P_Name_S TEXT NOT NULL\n" +
                    ");\n" +
                    "CREATE UNIQUE INDEX Permission_Lookup_P_Name_S_uindex ON Permission_Lookup (P_Name_S);\n" +
                    "CREATE UNIQUE INDEX Permission_Lookup_P_Name_uindex ON Permission_Lookup (P_Name);\n" +
                    "CREATE UNIQUE INDEX Permission_Lookup_P_id_uindex ON Permission_Lookup (P_id);";

            stm.execute(sql);

            sql = "CREATE TABLE IF NOT EXISTS Permissions\n" +
                    "(\n" +
                    "    User_id INTEGER NOT NULL,\n" +
                    "    P_id INTEGER NOT NULL,\n" +
                    "    CONSTRAINT permissions_p_id_user_id_pk PRIMARY KEY (P_id, User_id),\n" +
                    "    FOREIGN KEY (User_id) REFERENCES Users (ID) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,\n" +
                    "    FOREIGN KEY (P_id) REFERENCES Permission_Lookup (P_id) ON DELETE CASCADE ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED\n" +
                    ");\n" +
                    "CREATE UNIQUE INDEX Permissions_User_id_uindex ON Permissions (User_id);";

            stm.execute(sql);

            sql = "INSERT INTO Gender_Lookup (G_id, Gender_Name, Gender_Name_S) VALUES (1, 'ذكر', 'Male');\n" +
                    "INSERT INTO Gender_Lookup (G_id, Gender_Name, Gender_Name_S) VALUES (2, 'انثى', 'Female');";

            stm.execute(sql);

            sql = "INSERT INTO Permission_Lookup (P_id, P_Name, P_Name_S) VALUES (1, 'ادمن', 'Admin');\n" +
                    "INSERT INTO Permission_Lookup (P_id, P_Name, P_Name_S) VALUES (2, 'مستخدم', 'Customer');\n" +
                    "INSERT INTO Permission_Lookup (P_id, P_Name, P_Name_S) VALUES (3, 'مدخل بيانات', 'Data Entry');";

            stm.execute(sql);


            sql = "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (1, 'الأردن', 'JORDAN', 'الاردنية', 'JORDANIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (9, 'القدس', 'JERUSALEM', 'القدسية', 'JERUSALEMIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (10, 'فلسطين', 'PALESTINE', 'الفلسطينية', 'PALESTINIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (11, 'غير محدد', 'UNDEFINED', 'غير محدد', 'UNDEFINED');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (12, 'الناصرة', 'NAZARETH', 'الناصرة', 'NAZARETH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (13, 'مالي', 'MALI', 'الماليه', 'MALIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (14, 'مالطا', '-', 'مالطية', '-');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (15, 'صربيا', '-', 'صربية', '-');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (16, 'بلجيكا', '-', 'بلجيكية', '-');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (17, 'جنوب افريقيا', '-', 'جنوب افريقية', '-');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (18, 'بيرو', 'PERU', 'البيروفية', 'PERUVIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (19, 'غير كويتي', 'UNDEFINED', 'غير كويتي', 'UNDEFINED');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (20, 'بوسنا', 'BOSNA', 'البوسنية', 'BOSNI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (21, 'سوريا', 'SYRIA', 'السورية', 'SYRIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (22, 'لبنان', 'LEBANON', 'اللبنانية', 'LEBANESE');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (23, 'مصر', 'EGYPT', 'المصريه', 'EGYPTIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (24, 'العراق', 'IRAQ', 'العراقية', 'IRAQI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (25, 'السعودية', 'SAUDI ARABIA', 'السعودية', 'SAUDIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (26, 'الإمارات', 'U.A.E', 'الاماراتية', 'EMIRATI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (27, 'السودان', 'SUDAN', 'السودانية', 'SUDANESE');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (28, 'سلطنة عمان', 'OMAN', 'العمانية', 'OMANI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (29, 'اليمن', 'YEMEN', 'اليمنية', 'YEMENI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (30, 'ليبيا', 'LIBYA', 'الليبية', 'LIBYAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (31, 'البحرين', 'BAHRAIN', 'البحرينية', 'BAHRAINI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (32, 'الكويت', 'KUWAIT', 'الكويتية', 'KUWAITI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (33, 'قطر', 'QATAR', 'القطرية', 'QATARI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (34, 'تونس', 'TUNIS', 'التونسية', 'TUNISIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (35, 'المغرب', 'MOROCO', 'المغربيه', 'MOROCCO');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (36, 'تركيا', 'TURKY', 'التركية', 'TURKISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (37, 'افغانستان', 'AFGHANISTAN', 'افغانستانية', '-');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (38, 'ايران', 'IRAN', 'الايرانية', 'Iranian');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (39, 'باليز', 'BELIZE', 'الباليزية', 'BELIZAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (40, 'صين الشعبية', 'CHINA', 'الصينية', 'CHINESE');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (41, 'تايوان', 'TAIWAN', 'التايوانية', 'TAIWANIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (42, 'بنغلاديش', 'BANGHLADESH', 'البنغالية', 'BANGHALIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (43, 'بروناي', 'PRONAI', 'البرونية', 'PERUANA');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (44, 'بولندا', 'POLAND', 'البولندية', 'POLISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (45, 'سلوفانيا', 'SLOVANIA', 'السلوفانية', 'SLOVANIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (46, 'داغستان', 'DAGHISTAN', 'الداغستانية', 'DAGHISTANI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (47, 'اتحاد سوفيتي', 'U.S.S.R', 'السوفيتية', 'RUSSIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (48, 'دنمارك', 'DENMARK', 'الدنماركية', 'DANISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (49, 'رومانيا', 'ROMANIA', 'الرومانية', 'ROMANIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (50, 'جمهورية تشاد', 'CHAD', 'التشادية', 'CHADIENNE');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (51, 'الهند', 'INDIA', 'الهندية', 'INDIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (52, 'ماليزيا', 'MALAYZIA', 'الماليزية', 'MALAYIZIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (53, 'كازاخستان', 'KAZAKHISTAN', 'كازاخستانية', 'KAZAKHISTANI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (54, 'نيوزيلاندا', '-', 'النيوزلاندية', '-');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (55, 'أثيوبيا', 'ATHIOBIA', 'الاثيوبية', 'ATHIOBIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (56, 'أوغندا', 'OGANDA', 'الاوغندية', 'OGANDIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (57, 'راوندا', 'RWANDA', 'الراوندية', 'RAWANDIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (58, 'كوريا جنوبية', 'S. KOREA', 'الكورية ج.', 'S. KOREAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (59, 'النمسا', 'AUSTRIA', 'النمساوية', 'AUSTRIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (60, 'امريكا', 'U.S.A', 'الامريكية', 'AMERICAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (61, 'الباكستان', 'BAKISTAN', 'الباكستانية', 'PAKISTANI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (62, 'المانيا', 'GERMANY', 'الالمانية', 'DEUTSCH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (63, 'فرنسا', 'FRANCE', 'الفرنسية', 'FRENCH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (64, 'يوغسلافيا', 'YOUGISLAVYA', 'اليوغسلافية', 'YUGOSLAVIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (65, 'بلغاريا', 'BALGARYA', 'البلغارية', 'BULGARIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (66, 'الجزائر', 'ALGERIA', 'الجزائرية', 'ALGERIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (67, 'روسيا', 'RUSSIA', 'الروسية', 'RUSSIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (68, 'بريطانيا', 'BRITAIN', 'البريطانية', 'BRITISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (69, 'إيطاليا', 'ITALIA', 'الإيطالية', 'ITALIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (70, 'أندونيسيا', 'ANDONISIA', 'الاندونيسية', 'ANDONISIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (71, 'أستراليا', 'AUSTRALIA', 'الاسترالية', 'AUSTRALIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (72, 'كندا', 'CANADA', 'الكندية', 'CANADIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (73, 'كولومبيا', 'COLOMBIA', 'الكولومبية', 'COLOMBIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (74, 'البرازيل', 'BRAZIL', 'البرازيلية', 'BRAZILIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (75, 'الأورغوى', 'ORGWAY', 'الاورغوية', 'ORGWAY');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (76, 'فنزويلا', 'VENEZUELA', 'الفنزويلية', 'VENEZOLANA');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (77, 'أسبانيا', 'SPAIN', 'الاسبانية', 'SPANISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (78, 'السويد', 'SWEDEN', 'السويدية', 'SWEDISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (79, 'موريتانيا', 'MORITANIA', 'الموريتانية', 'MORITANI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (80, 'نيجيريا', 'NIJERYA', 'النيجيرية', 'NIJERIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (81, 'دومينيكان', 'DOMINICAN', 'الدومينيكاني', 'DOMINICANIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (82, 'كينيا', 'KENYA', 'الكينية', 'KENI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (83, 'سويسرا', 'SWITZERLAND', 'السويسرية', 'SWISS');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (84, 'تشيلي', 'CHILE', 'التشيلية', 'CHILEAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (85, 'بورتوريكو', 'PORT RICHO', 'البورتوريكية', 'PORT RICHI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (86, 'اوكرانيا', 'UKRAINE', 'الاوكرانية', 'UKRANIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (87, 'اليونان', 'GREECE', 'اليونانية', 'GREEK');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (88, 'اسرائيل', 'ISRAEL', 'الاسرائيلية', 'ISRAELIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (89, 'موريشوس', 'MAURITIUS', 'الموريشوسية', 'MAURITIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (90, 'كرواتيا', 'CROATIA', 'الكرواتية', 'CROATIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (91, 'هولندا', 'HOLAND', 'الهولندية', 'DUTCH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (92, 'الصومال', 'SOMAL', 'الصومالية', 'SOMALIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (93, 'هنغاريا', 'HUNGARIAN', 'الهنغارية', 'HUNGARIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (94, 'تايلندا', 'THAILAND', 'التايلنديه', 'THAI');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (95, 'ايرلندا', 'Irland', 'الايرلندية', 'IRISH');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (96, 'قبرص', 'CYPRUS', 'القبرصية', 'CYPRISIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (97, 'اثيوبيا', 'ETHEOPIAN', 'الاثيوبية', 'ETHEOPIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (98, 'الفلبين', 'PHILIPINES', 'الفلبينية', 'PHILIPINES');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (99, 'فنلندا', '-', 'فنلندية', 'Finalnd');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (100, 'ارتيريا', 'Eritrea', 'الارتيرية', 'Eritrian');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (101, 'بنما', 'PANAMA', 'البنمية', 'PANAMIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (102, 'سيريلانكا', 'SIR LANKA', 'السيريلانكيه', 'SIR LANKAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (103, 'سلفادور', 'SALVADOR', 'السلفادوريه', 'SALVADORIAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (104, 'الأرجنتين', 'ARGENTINA', 'الأرجنتينية', 'ARGENTINAN');\n" +
                    "INSERT INTO country (COUNTRY_NO, COUNTRY_NAME, COUNTRY_NAME_S, NATIONALITY, NATIONALITY_S) VALUES (105, 'جورجيا', 'GEORGIA', 'الجورجيه', 'GEORGIAN');";

            stm.execute(sql);
            sql = "INSERT INTO Users (ID, Email, Password, LastName, Phone, IsActive, Gender, Country, Age, FirstName) VALUES (1, 'alhusban.h@gmail.com', '0aac5f9a29335bfbb3a7054d576b1b93', 'Alhusban', '795937681', 1, 1, 1, 25, 'Hashim');";

            stm.execute(sql);
            sql = "INSERT INTO Permissions (User_id, P_id) VALUES (1, 1);";


            stm.execute(sql);


            tag = "Opened database successfully";

        } catch (SQLException e) {
            tag = e.toString();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        System.out.println(tag);

        return tag;
    }

}
