package com.example;

import com.example.Objects.User;
import com.example.Utilities.Tags;

import java.sql.*;

/**
 * Created by Hashim Al-husban
 * on 26/04/16.
 */
public class DataBase extends Tags {
    private static DataBase ourInstance = new DataBase();
    private Connection connection = null;

    private DataBase() {

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:d:\\test.db");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DataBase getInstance() {
        return ourInstance;
    }

    public synchronized int InsertUser(String Tag, String Values) {
        try {
            Statement stm = connection.createStatement();
            String sql = Tag + " " + Values;
            System.out.println(sql);
            stm.execute(sql);
            return 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public synchronized int UpdateUserInfo(User user) {
        try {
            Statement stm = connection.createStatement();
            String sql = Tags.USERINFOUPDATE;
            sql = String.format(sql, user.getFirstName(), user.getLastName(), user.getPhone(), user.getGender(), user.getCountry(), user.getLanguage(), user.getAge(), user.getEmail());
            return stm.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public synchronized int UpdateUser(User user) {
        try {
            Statement stm = connection.createStatement();
            String sql = Tags.USERUPDATE;
            sql = String.format(sql, user.getPassword(), user.getEmail());
            return stm.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public synchronized User GetUserInfo(String Email) {
        try {
            Statement stm = connection.createStatement();
            String sql = Tags.USERINFOSELECT;
            User u = new User();
            ResultSet rs = stm.executeQuery(sql.replace("_-_", Email));
            while (rs.next()) {
                u.setFirstName(rs.getString(USER_FIRSTNAME));
                u.setLastName(rs.getString(USER_LASTNAME));
                u.setEmail(rs.getString(USER_EMAIL));
                u.setPassword(rs.getString(USER_PASSWORD));
                u.setGender(rs.getInt(USER_GENDER));
                u.setCountry(rs.getInt(USER_COUNTRY));
                u.setAge(rs.getInt(USER_AGE));
                u.setPhone(rs.getString(USER_PHONE));
                u.setIsActive(rs.getInt(USER_ISACTIVE));
                u.setGenderName(rs.getString(USER_GENDERNAME));
                u.setGenderNameS(rs.getString(USER_GENDERNAMES));
                u.setCountryName(rs.getString(USER_COUNTRYNAME));
                u.setCountryNameS(rs.getString(USER_COUNTRYNAMES));
            }
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}
